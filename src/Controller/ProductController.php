<?php


namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductForm;
use App\Repository\ProductRepository;
use App\Repository\TownRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class ProductController extends AbstractController
{
    public function page(ProductRepository $repository, SessionInterface $session, Request $request)
    {

        $product = $repository->findAll();

        $selected = $request->query->get("select", null);
        $deselected = $request->query->get("deselect", null);

        if ($selected) {
            $id = intval($selected);
            // check if product exists
            if ($repository->find($id)) {
                // add to set of selected products
                $arr = $session->get("selected", []);
                $arr[$selected] = true;
                $session->set("selected", $arr);
            }
        }

        if ($deselected) {
            // remove from set of selected products
            $arr = $session->get("selected", []);
            unset($arr[$deselected]);
            $session->set("selected", $arr);
        }

        if (!empty($product)) {

            return $this->render('product.html.twig', ['products' => $product]);
        } else {
            return $this->redirectToRoute('app_home');
        }

    }




    public function edit($id, EntityManagerInterface $manager, Request $request, ProductRepository $repo)
    {

        $product = $repo->find($id);

        $form = $this->createForm(ProductForm::class, $product)
            ->add("Confirm_Changes", SubmitType::class);

        $form->handleRequest($request);


        if ($form->isSubmitted()) {

            $product = $form->getData();

            $manager->persist($product);
            $manager->flush();

            return $this->render('productdetail.html.twig', ["product" => $product]);
        }

        return $this->render('productedit.html.twig', ["form" => $form->createView(), 'product' => $product]);
    }

    public function delete(int $id, ProductRepository $repository, EntityManagerInterface $manager, Request $request, LoggerInterface $logger)
    {

        $product = $repository->find($id);

        if (!$product) {
            throw $this->createNotFoundException("Product with ID '$id' does not exist");
        }


        $manager->remove($product);
        $manager->flush();


        return $this->render('product.html.twig', [
            'product' => $product,
        ]);
    }

}