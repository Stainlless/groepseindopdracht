<?php


namespace App\Controller;

use App\Entity\Order;
use App\Entity\Orderline;
use App\Repository\ClientRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class BasketController extends AbstractController
{

    public function page(Request $request, ProductRepository $repository, SessionInterface $session, EntityManagerInterface $manager)
    {
        $error = $session->get("error", []);
        $addedproducts = $session->get("added", []);

        return $this->render('basket.html.twig', ['products' => $addedproducts, 'error' => $error,]);
    }

    public function add_to_basket(SessionInterface $session, Request $request, ProductRepository $repository, LoggerInterface $logger)
    {

        $id = $request->query->get("id", null);
        $addedproducts = $session->get("added", []);
        $productids= [];
        foreach ($addedproducts as $added){
            array_push($productids, $added->getId());

        }

        if ($id) {
            $product = $repository->find($id);

// can't add product if productnumber is already in basket
            if (
                $product and (!in_array($product->getId(), $productids))) {
                array_push($addedproducts, $product);
            }

            $session->set("added", $addedproducts);

            return $this->render('basket.html.twig', ['products' => $addedproducts]);

        }

        $selected = $session->get("selected", []);
        if ($selected) {
            $addedproducts = $session->get("added", []);

            $products = $repository->findAll();
            foreach ($products as $product) {
                if (in_array($product->getID(), array_keys($selected)) and (!in_array($product->getId(), $productids))) {
                    if ($product) {
                        array_push($addedproducts, $product);
                    }
                }
            }

            $selected = [];
            $session->set("added", $addedproducts);
            $session->set("selected", $selected);

            return $this->render('basket.html.twig', ['products' => $addedproducts]);
        } else {
            return $this->render('basket.html.twig');
        }
    }

    public function delete_from_basket(int $id, SessionInterface $session, Request $request, ProductRepository $repository)
    {
        $addedproducts = $session->get("added", []);

        if ($addedproducts) {
            unset($addedproducts[$id]);;

            $session->set("added", $addedproducts);

            return $this->render('basket.html.twig', ['products' => $addedproducts]);
        } else {

            return $this->render('home.html.twig');
        }


    }

    public function clear_basket(SessionInterface $session, UserRepository $user, Request $request, ProductRepository $repository)
    {

        $added = $session->get("added", []);
        $ordered = $session->get("ordered");
        $total = $session->get("total");
        $aantal = $session->get("aantallen");
        unset($added);
        unset($ordered);
        unset($total);
        unset($aantal);
        $session->set("added", []);
        $session->set("ordered", []);
        $session->set("total", []);
        $session->set("aantallen", []);

        return $this->render('basket.html.twig');
    }


    public function confirm(Request $request, UserRepository $user, ClientRepository $clientrepository, SessionInterface $session, EntityManagerInterface $manager)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');


        $addedproducts = $session->get("added", []);
        $orderedproducts = $session->get("ordered", []);
        $totalprice = $session->get("total");
        // process form input

        if ($user and !empty($addedproducts)) {
            $aantallen = [];
            $order = new Order();
            $user = $this->getUser();
            $totalprice = 0;
            $client = $user->getClient();
            $order->setClient($client);
            $order->setStatus("confirmed");

            $i = 0;
            foreach ($addedproducts as $key => $product) {

                $orderline = new Orderline();
                $aantal = $request->request->get('quantity' . $product->getID());
                if (!$aantal) {
                    $aantalarr = $session->get("aantallen");
                    if(!$aantalarr[$i]){
                        $aantal = 1;
                    } else {
                    $aantal = $aantalarr[$i];
                    }
                }
                $i++;
                array_push($aantallen, $aantal);
                $orderline->setAantal($aantal);
                $stock = $product->getStock();
                $currentstock = $stock->getQuantity();
                $orderline->setProduct($product);
                $order->addOrderline($orderline);
                $totalprice = $totalprice + ($aantal * $product->getPrijs());

            }
            $session->set("ordered", $order);
            $session->set("aantallen", $aantallen);
            $session->set("total", $totalprice);

            // redirect to orderconfirmation page

            return $this->render('orderconfirmation.html.twig', ['orderdetails' => $order, 'aantal' => $aantallen, 'prijs' => $totalprice]);

        } elseif (!$user) {
            return $this->redirectToRoute('app_login');
        } else {
            return $this->render('basket.html.twig');
        }
    }

}