<?php

namespace App\Controller;

use App\Entity\Location;
use App\Repository\ActionRepository;
use App\Repository\LocationRepository;
use App\Repository\TownRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AdminController extends AbstractController
{
    public function page(SessionInterface $session)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        return $this->render('admin.html.twig');
    }

    public function userlist(UserRepository $userRepository)
    {

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $users = $userRepository->findByRole('USER');

        return $this->render('admin_userlist.html.twig', [
            'users' => $users
        ]);
    }

    public function block_user(int $id, UserRepository $userRepository, EntityManagerInterface $manager)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $user = $userRepository->find($id);
        if ($user->getIsblocked() == false) {
            $user->setIsblocked(true);
        } else {
            $user->setIsblocked(false);
        }
        $manager->persist($user);
        $manager->flush();

        return $this->redirectToRoute('app_admin_userlist');
    }

    public function resetspeelveld(TownRepository $townRepository, ActionRepository $actionRepository, LocationRepository $locationRepository, Request $request, EntityManagerInterface $manager)
    {
        if ($request->getMethod() == 'POST') {
            $width = $request->request->get('width');
            $height = $request->request->get('height');

            if ($width and $height) {

                if ($width > 9 and $width < 21 and $height > 9 and $height < 51) {

                    $towns = $townRepository->findAll();
                    $actions = $actionRepository->findAll();
                    $locations = $locationRepository->findAll();

                    foreach ($towns as $town) {
                        $manager->remove($town);
                    }
                    foreach ($actions as $action) {
                        $manager->remove($action);
                    }
                    foreach ($locations as $location) {
                        $manager->remove($location);
                    }
                    $manager->flush();


                    //nieuw speelveld aanmaken
                    for ($i = 0; $i < $height; $i++) {
                        for ($j = 0; $j < $width; $j++) {
                            $location = new Location();
                            $location->setLocationx($j);
                            $location->setLocationy($i);
                            $manager->persist($location);
                            $manager->flush();
                        }
                    }
                    return $this->redirectToRoute('app_home');
                } else {

                    $error = 'Invalid width and/or height';

                    return $this->render('admin.html.twig', [
                        'error' => $error
                    ]);

                }
            } else {

                for ($i = 0; $i < 20; $i++) {
                    for ($j = 0; $j < 20; $j++) {
                        $location = new Location();
                        $location->setLocationx($i);
                        $location->setLocationy($j);
                        $manager->persist($location);
                        $manager->flush();
                    }
                }
                return $this->redirectToRoute('app_home');

            }
        } else {

            return $this->render('admin_resetspeelveld.html.twig');
        }
    }
}