<?php

namespace App\Controller;

use App\Entity\Action;
use App\Entity\Town;
use App\Repository\LocationRepository;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Location;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\TownRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\User\UserInterface;


class GameController extends AbstractController
{
    public function checklocation(int $locationx, int $locationy, LocationRepository $Repository)
    {
        $checktown = false;
        for ($x = ($locationx - 1); $x <= ($locationx + 1); $x++) {

            for ($y = ($locationy - 1); $y <= ($locationy + 1); $y++) {
                $checklocation = $Repository->findOneBy([
                    'locationx' => $x,
                    'locationy' => $y
                ]);
                if ($checklocation !== null) {
                    if (!empty($checklocation->getTown())) {
                        $checktown .= true;
                    }
                }
            }
        }
        return $checktown;
    }

    public function buildfirsttown(UserInterface $user, TownRepository $townRepository, LocationRepository $locationRepository, EntityManagerInterface $manager, Request $request, SessionInterface $session)
    {
        if ($request->getMethod() == 'POST') {
            $session->remove('error');
            $session->remove('result');
            $session->remove('defend');
            $session->remove('build');
            $error = '';
            $action = new Action();
            $town = new Town();

            $locationId = $request->request->get('locationId');
            $location = $locationRepository->find($locationId);
            $locationy = $location->getLocationy();
            $locationx = $location->getLocationx();

            //eerst controlerer we of er geen town rondom of op de gekozen locatie staat
            if ($this->checklocation($locationx, $locationy, $locationRepository) == true) {
                $error = 'De gekozen locatie heeft al een town of er is al een town naast. ';
            } else {

                if ($this->checklocation($locationx, $locationy, $locationRepository) == false) {

                    //nu maken we de town aan en worden er 20 units meegegeven
                    $givenunits = 20;
                    $town->setUser($user);
                    $town->setLocation($locationRepository->findOneBy(['locationx' => $locationx, 'locationy' => $locationy]));
                    $town->setTownquantity($givenunits);
                    $town->setTownstatus('First town created');
                    $manager->persist($town);
                    $manager->flush();
                    $action->setActiondescription('First town was built');
                    $action->setActionquantity($givenunits);
                    $action->setLocation($location);
                    $action->setTown($town);
                    $action->setUser($user);
                    $manager->persist($action);
                    $manager->flush();

                    $session->set('first town built', [$user, $givenunits, $locationx, $locationy, $town, $action]);
                } else {
                    $error = "Not allowed";

                }
            }
            $session->set('error', $error);
        }
        return $this->redirectToRoute('app_home');
    }


    public function createunits(UserInterface $userint, UserRepository $userRepository, TownRepository $townRepository, EntityManagerInterface $manager)
    {
        //eerste wordt de logindatum vergeleken met de database als het de volgende dag is krijgt de speler 14 units per town bij
        //anders krijgt de speler vast 10 units per town per dag
        $user = $userRepository->find($userint);
        $lastlogindate = $user->getLogindate();

        //dit is het stukje dat het verschil in twee datum berekent in dagen
        $newlogindate = new \DateTime(date("Y-m-d"));
        $datediff = date_diff($lastlogindate, $newlogindate);
        $datediffindays = $datediff->format("%a");


        if ($datediffindays == 1) {
            //het is de eerst volgende dag, de speler krijgt 14 units per town daarna updaten we de lastlogin
            $towns = $townRepository->findBy(['user' => $user]);

            foreach ($towns as $town) {
                $oldtownquantity = $town->getTownquantity();
                $newtownquantity = $oldtownquantity + 14;
                $town->setTownquantity($newtownquantity);
                $manager->persist($town);
                $manager->flush();
            }
            $user->setLogindate($newlogindate);

            $manager->persist($user);
            $manager->flush();

        }
        if ($datediffindays > 1) {
            //het is meer dan een dag geleden, de speler krijgt 10 units per town per dag
            $towns = $townRepository->findBy(['user' => $user]);

            foreach ($towns as $town) {
                $oldtownquantity = $town->getTownquantity();
                $newtownquantity = $oldtownquantity + ($datediffindays * 10);
                $town->setTownquantity($newtownquantity);
                $manager->persist($town);
                $manager->flush();
            }
            $user->setLogindate($newlogindate);

            $manager->persist($user);
            $manager->flush();
        }
//hello test
        return $this->redirectToRoute('app_home');
    }

    public function createTown(UserInterface $userint, UserRepository $userRepository, TownRepository $townRepository, LocationRepository $locationRepository, EntityManagerInterface $manager, Request $request, SessionInterface $session)
    {
        /**
         * bestaat gekozen locatie wel?
         * locatie checken (heeft al user of is niet leeg rondom)
         * checken of de town waarvan de units moeten komen ook minstens 12 units heeft (town mag niet leeg zijn en het createn kost 10 units=> 12-10=2 =>1 in oorspronkelijke town en 1 in nieuwe town)
         * gekozen units aftrekken van townquantity oorspronkelijke town
         * town createn kost 10 units!: setuser to town met de quantity-10!
         */
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($request->getMethod() == 'POST') {
            $session->remove('error');
            $session->remove('result');
            $session->remove('defend');
            $session->remove('build');
            $error = '';
            $action = new Action();
            $town = new Town();

            $locationId = $request->request->get('locationId');
            $location = $locationRepository->find($locationId);
            $locationy = $location->getLocationy();
            $locationx = $location->getLocationx();

            $buildunits = $request->request->get('buildunits');

            //controleren of town genoeg buildunits heeft
            if ($buildunits < 11) {
                $error .= 'Je hebt niet genoeg units gekozen om een town te zetten. ';
            }

            $origintown = $request->request->get('origintown');
            //controleren of origintown ook echt van ingelogde user is
            $usertown = $townRepository->findOneBy(['id' => $origintown]);
            if (null != $usertown) {
                $townquantity = $usertown->getTownquantity();
                if ($townquantity <= $buildunits) {
                    $error .= 'Je hebt niet genoeg units om een town te zetten. ';
                }
                if (!in_array($usertown, $userint->getTowns()->toArray())) {
                    $error .= "De town waarvan je units stuurt is niet van jou! Leuk geprobeerd, snoodaard! ";
                }
            }

            //eerst controlerer we of er geen town rondom of op de gekozen locatie staat
            if ($this->checklocation($locationx, $locationy, $locationRepository) == true) {
                $error .= 'De gekozen locatie heeft al een town of er is al een town naast. ';
            }

            if ($error == '') {
                //nu maken we de town aan en worden het gekozen aantal units meegestuurd
                $town->setUser($userint);
                $town->setLocation($location);
                $lostunits = round((rand(1, 5) * $buildunits / 100));
                $newunits = $buildunits - $lostunits;
                $town->setTownquantity($newunits);
                $town->setTownstatus('Town Built');
                $usertown->setTownquantity($townquantity - 20);
                $manager->persist($town);
                $action->setActiondescription('Build');
                $action->setActionquantity($buildunits);
                $action->setLocation($location);
                $action->setUser($userint);
                $manager->persist($action);
                $manager->flush();

                $session->set('build', [$buildunits, $origintown, $locationx, $locationy, $lostunits, $newunits, $usertown, $action]);
            } else {
                $session->set('error', $error);

            }
        }
        return $this->redirectToRoute('app_home');
    }

    public function attacktown(UserInterface $userint, UserRepository $userRepository, TownRepository $townRepository, LocationRepository $locationRepository, EntityManagerInterface $manager, Request $request, SessionInterface $session)
    {

        /**
         * staat op de gekozen locatie een town van een andere gebruiker?
         * minimum 1 eenheid om aan te vallen, maximum (origintownquantity - 1)
         * gekozen units aftrekken van townquantity oorspronkelijke town
         * berekening winnaar van de aanval.
         * opslaan en weergave resultaat
         */
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($request->getMethod() == 'POST') {
            $session->remove('error');
            $session->remove('result');
            $session->remove('defend');
            $session->remove('build');
            $error = '';
            $action = new Action();

            $locationId = $request->request->get('locationId');

            $location = $locationRepository->find($locationId);
            $locationy = $location->getLocationy();
            $locationx = $location->getLocationx();

            $originunits = $request->request->get('attackunits');

            //controleren of town genoeg attackunits heeft
            if ($originunits < 1) {
                $error .= 'Je moet minstens 1 unit sturen';
            }

            $origintown = $request->request->get('origintown');

            //controleren of origintown ook echt van ingelogde user is
            $usertown = $townRepository->find($origintown);
            $townquantity = $usertown->getTownquantity();
            if ($townquantity <= $originunits) {
                $error .= 'Je hebt niet genoeg units om deze actie te doen';
            }
            if (!in_array($usertown, $userint->getTowns()->toArray())) {
                $error .= "De town waarvan je units stuurt is niet van jou! Leuk geprobeerd, snoodaard!";
            }

            //controleren of er de aangevallen town bestaat en van een andere user is
            $targettown = $location->getTown();
            $targetuser = $targettown->getUser();
            if (!$targettown) {
                $error .= 'Er is geen town op deze locatie om aan te vallen';
            }
            if ($targettown and $targetuser == $userint) {
                $error .= 'Je kan je eigen town niet aanvallen';
            }

            if ($error == '') {
                $success = false;
                //verminderen uitgestuurde aanheden in town
                $usertown->setTownquantity($townquantity - $originunits);

                // berekening verlies eenheden in functie van afstand
                $originlocation = $usertown->getLocation();
                $originlocationx = $originlocation->getLocationx();
                $originlocationy = $originlocation->getLocationy();
                $distance = sqrt(pow($originlocationx - $locationx, 2) + pow($originlocationy - $locationy, 2));

                // verlies van eenheden op basis van 2 factoren:
                // 1. random units
                // vermenigvuldigd met
                // 2. afstand van de towns in verhouding met de grootst mogelijke afstand.
                $unitslost = round(($originunits / ((rand(15, 50) / 10) * (1 + ((sqrt(800) - $distance) / sqrt(800))))));

                $attackunits = $originunits - $unitslost;

                // De winnaar wordt berekend
                $targetquantity = $targettown->getTownquantity();
                $no_usertowns = count($userint->getTowns());
                // De aanvaller wint.
                //berekening verdediging // getallen zijn voorlopig kunnen nog getweakt woden voor de spel balans
                $defendunits = rand(70, 130) / 100 * $targetquantity + 1;
                //berekening aanvalskracht (spel belans: als je veel towns hebt wordt je aanvalskracht iets minder)
                $calculated_attackunits = ($attackunits * ((200 - $no_usertowns) * rand(20, 220) / 100) / 200);
                //berekening resterende eenheden na confrontatie

                $action->setActiondescription('Attack');
                $action->setActionquantity($originunits);
                $action->setLocation($location);
                $action->setTown($usertown);
                $action->setUser($userint);
                $manager->persist($action);

                if ($calculated_attackunits > $defendunits) {
                    $result = true;
                    //de aanval is succesvol.
                    $leftoverunits = round(1 + abs($attackunits - $defendunits));
                    $targettown->setUser($userint);
                    $targettown->setTownquantity($leftoverunits);
                    $targettown->setTownstatus('Conquered by ' . $userint->getEmail());

                    $manager->persist($targettown);
                    $manager->persist($usertown);

                    $action->setUser($userint);

                } else {
                    $result = false;
                    // De verdediging houdt stand.
                    $leftoverunits = round(1 + abs($targetquantity - $attackunits));
                    $targettown->setTownstatus('Failed attack by ' . $userint->getEmail());
                    $targettown->setTownquantity($leftoverunits);

                    $manager->persist($usertown);
                    $manager->persist($targettown);

                }
                $manager->flush();
                //het resultaat wordt meegegeven
                //
                $actionCountOld = $userint->getActionCount();
                $actionCountNew = $actionCountOld +1;
                $userint->setActionCount($actionCountNew);

                if($actionCountNew == 20){
                    $coinsOld = $userint->getCoins();
                    $userint->setCoins($coinsOld + 5);
                    $userint->setActionCount(0);
                }
                $session->set('result',[$result, $originunits, $usertown, $targetquantity, $targettown, $leftoverunits, $action, $unitslost] );


            } else {
                $session->set('error', $error);
            }
        }
        return $this->redirectToRoute('app_home');
    }

    public function defendtown(UserInterface $userint, UserRepository $userRepository, TownRepository $townRepository, LocationRepository $locationRepository, EntityManagerInterface $manager, Request $request, SessionInterface $session)
    {

        /**
         * staat op de gekozen locatie een town van een andere gebruiker?
         * minimum 1 eenheid om aan te vallen, maximum (origintownquantity - 1)
         * gekozen units aftrekken van townquantity oorspronkelijke town
         * berekening winnaar van de aanval.
         * opslaan en weergave resultaat
         */
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($request->getMethod() == 'POST') {
            $session->remove('error');
            $session->remove('result');
            $session->remove('defend');
            $session->remove('build');
            $error = '';
            $action = new Action();

            $locationId = $request->request->get('locationId');

            $location = $locationRepository->find($locationId);
            $locationy = $location->getLocationy();
            $locationx = $location->getLocationx();

            $originunits = $request->request->get('defendunits');

            //controleren of town genoeg attackunits heeft
            if ($originunits < 1) {
                $error .= 'Je moet minstens 1 unit sturen';
            }

            $origintown = $request->request->get('origintown');

            //controleren of origintown ook echt van ingelogde user is
            $usertown = $townRepository->find($origintown);
            $townquantity = $usertown->getTownquantity();
            if ($townquantity <= $originunits) {
                $error .= 'Je hebt niet genoeg units om deze actie te doen';
            }
            if (!in_array($usertown, $userint->getTowns()->toArray())) {
                $error .= "De town waarvan je units stuurt is niet van jou! Leuk geprobeerd, snoodaard!";
            }

            //controleren of er de aangevallen town bestaat en van een andere user is
            $targettown = $location->getTown();
            $targetuser = $targettown->getUser();
            if (!$targettown) {
                $error .= 'Er is geen town op deze locatie om aan te vallen';
            }
            if ($targettown and $targetuser != $userint) {
                $error .= 'Je kan enkel je eigen town verdedigen';
            }

            if ($error == '') {
                $success = false;
                //verminderen uitgestuurde aanheden in town
                $usertown->setTownquantity($townquantity - $originunits);

                // berekening verlies eenheden in functie van afstand
                $originlocation = $usertown->getLocation();
                $originlocationx = $originlocation->getLocationx();
                $originlocationy = $originlocation->getLocationy();
                $distance = sqrt(pow($originlocationx - $locationx, 2) + pow($originlocationy - $locationy, 2));

                // verlies van eenheden op basis van 2 factoren:
                // 1. random units
                // vermenigvuldigd met
                // 2. afstand van de towns in verhouding met de grootst mogelijke afstand.
                $unitslost = round($originunits / ((rand(10, 25) / 10) * $originunits * (1 + $distance / (sqrt(800)))));

                $defendunits = $originunits - $unitslost;
                $targetquantity = $targettown->getTownquantity();
                $newunits = $defendunits + $targetquantity;

                $action->setActiondescription('Defend');
                $action->setActionquantity($originunits);
                $action->setLocation($location);
                $action->setTown($usertown);
                $action->setUser($userint);
                $manager->persist($action);

                $targettown->setTownquantity($newunits);
                $targettown->setTownstatus('Town reinforced');
                $manager->persist($targettown);
                $manager->persist($usertown);

                $manager->flush();
                //het resultaat wordt meegegeven


                $actionCountOld = $userint->getActionCount();
                $actionCountNew = $actionCountOld +1;
                $userint->setActionCount($actionCountNew);

                if($actionCountNew == 20){
                    $coinsOld = $userint->getCoins();
                    $userint->setCoins($coinsOld + 5);
                    $userint->setActionCount(0);
                }

                $manager->flush();
                $session->set('defend',[$originunits, $usertown, $targettown, $targetquantity, $action, $newunits, $unitslost] );

            } else {
                $session->set('error', $error);
            }
        }
        return $this->redirectToRoute('app_home');
    }
}
