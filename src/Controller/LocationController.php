<?php

namespace App\Controller;

use App\Entity\Location;
use App\Repository\LocationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class LocationController extends AbstractController
{

    public function createGrid(LocationRepository $repository, Request $request)
    {
        //$locations = $repository->findAll();
        $arrayID=[];
        for ($i = 0; $i < 20; $i++) {
            $arrlocation=[];
            for ($j = 0; $j < 20; $j++) {
                $locationId = ($repository->findOneBy(['locationx' => $i,'locationy'=>$j]))->getId();
                //var_dump($locationId);
                array_push($arrlocation, $locationId);
                }
            array_push($arrayID,$arrlocation);
        }
        return $this->render('create_grid.html.twig', ['locations' => $arrayID]);
    }



}