<?php

namespace App\Controller;

use App\Repository\TownRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ProfileController extends AbstractController
{

    public function page()
    {
        return $this->render('profile/index.html.twig');
    }

    function random_str(
        $length = 12,
        $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ) {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        if ($max < 1) {
            throw new Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

    function del_user($id, EntityManagerInterface $entityManager,UserInterface $user,UserRepository $userRepository, TownRepository  $townRepository,UserPasswordEncoderInterface $passwordEncoder)
    {

//        $em = $this->getDoctrine()->getEntityManager();
//        $Userid = $userRepository->find($id);
        //  $user = $userRepository->find($userinterface);
        $userRole = $user->getRoles();
        if ($userRole == ["ROLE_USER"]) {
            $hashedPassword = $passwordEncoder->encodePassword($user, $this->random_str());
            $user->setPassword($hashedPassword);
            $user->setUsername('Verwijderd');
            $user->setEmail('onbekend' . $id . '@onbekend.be');
            $towns = $user->getTowns();


            $userBarb = $userRepository->findOneBy(['username'=>'Barbaar']);


            foreach ($towns as $town) {
                $town->setTownstatus('free for all');
                $town->setUser($userBarb);
                $entityManager->persist(($town));
                $entityManager->flush();
            }

//      $entityManager->remove($Userid);
            $entityManager->flush();

// zelf deleten alles anonimiseren geblokeerd worden niks anonimiseren en enkel de login weigeren.
            return $this->redirectToRoute('app_logout');
        }
        return $this->redirectToRoute('app_profile');
    }
}
