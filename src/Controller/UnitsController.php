<?php

namespace App\Controller;

use App\Entity\Action;
use App\Entity\Product;
use App\Entity\Town;
use App\Form\ProductForm;
use App\Repository\ProductRepository;
use App\Repository\TownRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UnitsController extends AbstractController
{
    public function page(ProductRepository $repository, SessionInterface $session, Request $request)
    {

        $product = $repository->findAll();

        $selected = $request->query->get("select", null);
        $deselected = $request->query->get("deselect", null);

        if ($selected) {
            $id = intval($selected);
            // check if product exists
            if ($repository->find($id)) {
                // add to set of selected products
                $arr = $session->get("selected", []);
                $arr[$selected] = true;
                $session->set("selected", $arr);
            }
        }

        if ($deselected) {
            // remove from set of selected products
            $arr = $session->get("selected", []);
            unset($arr[$deselected]);
            $session->set("selected", $arr);
        }

        if (!empty($product)) {

            return $this->render('product.html.twig', ['products' => $product]);
        } else {
            return $this->redirectToRoute('app_home');
        }

    }




    public function edit($id, EntityManagerInterface $manager, Request $request, ProductRepository $repo)
    {

        $product = $repo->find($id);

        $form = $this->createForm(ProductForm::class, $product)
            ->add("Confirm_Changes", SubmitType::class);

        $form->handleRequest($request);


        if ($form->isSubmitted()) {

            $product = $form->getData();
            $manager->persist($product);
            $manager->flush();

            return $this->render('productdetail.html.twig', ["product" => $product]);
        }

        return $this->render('productedit.html.twig', ["form" => $form->createView(), 'product' => $product]);
    }

    public function delete(int $id, ProductRepository $repository, EntityManagerInterface $manager, Request $request, LoggerInterface $logger)
    {

        $product = $repository->find($id);

        if (!$product) {
            throw $this->createNotFoundException("Product with ID '$id' does not exist");
        }


        $manager->remove($product);
        $manager->flush();


        return $this->render('product.html.twig', [
            'product' => $product,
        ]);
    }
//find user_id om zo te towns van de ingelogde user te zoeken
//geef towns weer in dropdown
    public function chooseTown( TownRepository $repository, ProductRepository  $productRep, Request $request, SessionInterface $session, EntityManagerInterface $manager,UserInterface $user)
    {

     $coins = $user->getCoins();
        if ($request->getMethod() == 'POST') {
            $session->remove('error');
            $session->remove('result');
            $session->remove('defend');
            $session->remove('build');
            $error = '';
            $action = new Action();

            $townId = $request->request->get('chosenTown');
            $town = $repository->find($townId);
            $productId = $request->request->get('productId');
            $product = $productRep->find($productId);
            $product = $productRep->find($productId);
            $productPrice = $product->getPrice();

            if ($productPrice <= $coins) {
                $townLocation = $town->getLocation();
                $townQuantityOld = $town->getTownquantity();
                $townQuantityNew = $townQuantityOld + 20;
                $town->setTownquantity($townQuantityNew);
                $manager->persist($town);
                $manager->flush();
                $action->setActiondescription('Units bought');
                $action->setActionquantity(20);
                $action->setTown($town);
                $action->setUser($user);
                $action->setLocation($townLocation);
                $manager->persist($action);
                $manager->flush();

                $coinsOld = $user->getCoins();
                $user->setCoins($coinsOld - $productPrice);
                $manager->persist($user);
                $manager->flush();
                $success = "20 units were added to your town";
                $session->set("success", $success);
            } else {
                $error = 'Je hebt niet genoeg coins';
                $session->set("error", $error);
            }
        }
        return $this->redirectToRoute('app_basket');
    }

}