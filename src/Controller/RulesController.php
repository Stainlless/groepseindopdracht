<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RulesController extends AbstractController
{
    public function page()
    {
        return $this->render('rules.html.twig');
    }

}