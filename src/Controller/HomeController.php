<?php

namespace App\Controller;

use App\Entity\Town;
use App\Entity\Location;
use App\Repository\ActionRepository;
use App\Repository\LocationRepository;
use App\Repository\TownRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\Query\Expr\Math;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class HomeController extends AbstractController
{
    public function page(LocationRepository $locationrepository, UserRepository $userRepository, ActionRepository $actionRepository, SessionInterface $session)
    {

        $width=count($locationrepository->findBy(['locationy'=>true]));
        $height=count($locationrepository->findBy(['locationx'=>true]));

        $events = $actionRepository->findALL();
        rsort($events);
        $events = array_slice($events, 0, 6);

        $userscores = [];
        $users = $userRepository->findAll();
        foreach ($users as $user) {

            $username = $user->getUsername();
            $towns = $user->getTowns();
            $actions = $user->getActions();
            $numberofactions = count($actions);
            $numberoftowns = count($towns) ;
            $totalquantity = 0;
            foreach ($towns as $town) {
                $quantity = $town->getTownquantity();
                $totalquantity = $totalquantity + $quantity;
            }
            $score = round(($totalquantity * 0.75 * (5 * (1 + $numberoftowns)) / (4) * (1 + ($numberofactions/(1+($numberofactions/(1 + $numberoftowns))/100))) / 25), 2);
            array_push($userscores, [$username, $totalquantity, $numberoftowns, $numberofactions, $score]);
        };
        array_multisort(array_column($userscores, 4), SORT_DESC, $userscores);

        $arrlocation = [];
        for ($i = 0; $i < $height; $i++) {
            $arrloc = [];
            for ($j = 0; $j < $width; $j++) {
                $location = ($locationrepository->findOneBy(['locationx' => $j, 'locationy' => $i]));
                $action = $location->getActions();
                $town = $location->getTown();
                // $town = $location->getTown();

                if ($town) {
                    $actions = $town->getActions();
                    foreach ($actions as $action) {
                        $town->addAction($action);
                    }
                    $user = $town->getUser();
                    if ($user) {
                        $userTowns = $user->getTowns();
                        foreach ($userTowns as $userTown) {
                            $user->addTown($userTown);
                        }
                    }
                    array_push($arrloc, [$location, $action, $town, $user]);
                } else {

                    array_push($arrloc, [$location, $action, ' ', ' ']);
                }

            }
            array_push($arrlocation, $arrloc);
        }

        return $this->render('home.html.twig', ['cells' => $arrlocation, 'events' => $events, 'scores' => $userscores]);;
    }

}
