<?php

namespace App\Entity;

use App\Repository\ActionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ActionRepository::class)
 */
class Action
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $actionquantity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $actiondescription;

    /**
     * @ORM\ManyToOne(targetEntity=Location::class, inversedBy="actions")
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity=Town::class, inversedBy="actions")
     */
    private $town;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="actions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getActionquantity(): ?int
    {
        return $this->actionquantity;
    }

    public function setActionquantity(int $actionquantity): self
    {
        $this->actionquantity = $actionquantity;

        return $this;
    }

    public function getActiondescription(): ?string
    {
        return $this->actiondescription;
    }

    public function setActiondescription(string $actiondescription): self
    {
        $this->actiondescription = $actiondescription;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getTown(): ?Town
    {
        return $this->town;
    }

    public function setTown(?Town $town): self
    {
        $this->town = $town;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
