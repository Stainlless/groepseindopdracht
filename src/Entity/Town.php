<?php

namespace App\Entity;

use App\Repository\TownRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TownRepository::class)
 */
class Town
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $townstatus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $townquantity;

    /**
     * @ORM\OneToOne(targetEntity=Location::class, inversedBy="town", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="Towns")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Action::class, mappedBy="town")
     */
    private $actions;

    public function __construct()
    {
        $this->actions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTownstatus(): ?string
    {
        return $this->townstatus;
    }

    public function setTownstatus(?string $townstatus): self
    {
        $this->townstatus = $townstatus;

        return $this;
    }

    public function getTownquantity(): ?int
    {
        return $this->townquantity;
    }

    public function setTownquantity(?int $townquantity): self
    {
        $this->townquantity = $townquantity;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Action[]
     */
    public function getActions(): Collection
    {
        return $this->actions;
    }

    public function addAction(Action $action): self
    {
        if (!$this->actions->contains($action)) {
            $this->actions[] = $action;
            $action->setTown($this);
        }

        return $this;
    }

    public function removeAction(Action $action): self
    {
        if ($this->actions->removeElement($action)) {
            // set the owning side to null (unless already changed)
            if ($action->getTown() === $this) {
                $action->setTown(null);
            }
        }

        return $this;
    }

}
