<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="date")
     */
    private $logindate;
    
    /**
     * @ORM\OneToMany(targetEntity=Town::class, mappedBy="user")
     */
    private $Towns;

    /**
     * @ORM\OneToMany(targetEntity=Action::class, mappedBy="user")
     */
    private $actions;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isblocked;

    /**
     * @ORM\Column(type="integer")
     */
    private $coins;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $actionCount;

    public function __construct()
    {
        $this->Towns = new ArrayCollection();
        $this->actions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getCoins(): ?int
    {
        return $this->coins;
    }
    public function setCoins(string $coins): self
    {
        $this->coins = $coins;

        return $this;
    }
    public function getActionCount(): ?int
    {
        return $this->actionCount;
    }
    public function setActionCount(string $actionCount): self
    {
        $this->actionCount = $actionCount;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }



    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }


    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getLogindate(): ?\DateTimeInterface
    {
        return $this->logindate;
    }

    public function setLogindate(\DateTimeInterface $logindate): self
    {
        $this->logindate = $logindate;
        
        return $this;
    }
    
    /**
     * @return Collection|Town[]
     */
    public function getTowns(): Collection
    {
        return $this->Towns;
    }

    public function addTown(Town $town): self
    {
        if (!$this->Towns->contains($town)) {
            $this->Towns[] = $town;
            $town->setUser($this);
        }

        return $this;
    }

    public function removeTown(Town $town): self
    {
        if ($this->Towns->removeElement($town)) {
            // set the owning side to null (unless already changed)
            if ($town->getUser() === $this) {
                $town->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Action[]
     */
    public function getActions(): Collection
    {
        return $this->actions;
    }

    public function addAction(Action $action): self
    {
        if (!$this->actions->contains($action)) {
            $this->actions[] = $action;
            $action->setUser($this);
        }

        return $this;
    }

    public function removeAction(Action $action): self
    {
        if ($this->actions->removeElement($action)) {
            // set the owning side to null (unless already changed)
            if ($action->getUser() === $this) {
                $action->setUser(null);
            }
        }

        return $this;
    }

    public function getIsblocked(): ?bool
    {
        return $this->isblocked;
    }

    public function setIsblocked(bool $isblocked): self
    {
        $this->isblocked = $isblocked;

        return $this;
    }
}
