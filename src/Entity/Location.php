<?php

namespace App\Entity;

use App\Repository\LocationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LocationRepository::class)
 */
class Location
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $locationx;

    /**
     * @ORM\Column(type="integer")
     */
    private $locationy;

    /**
     * @ORM\OneToMany(targetEntity=Action::class, mappedBy="location")
     */
    private $actions;

    /**
     * @ORM\OneToOne(targetEntity=Town::class, mappedBy="location", cascade={"persist", "remove"})
     */
    private $town;

    public function __construct()
    {
        $this->actions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocationx(): ?int
    {
        return $this->locationx;
    }

    public function setLocationx(int $locationx): self
    {
        $this->locationx = $locationx;

        return $this;
    }

    public function getLocationy(): ?int
    {
        return $this->locationy;
    }

    public function setLocationy(int $locationy): self
    {
        $this->locationy = $locationy;

        return $this;
    }

    /**
     * @return Collection|Action[]
     */
    public function getActions(): Collection
    {
        return $this->actions;
    }

    public function addAction(Action $action): self
    {
        if (!$this->actions->contains($action)) {
            $this->actions[] = $action;
            $action->setLocation($this);
        }

        return $this;
    }

    public function removeAction(Action $action): self
    {
        if ($this->actions->removeElement($action)) {
            // set the owning side to null (unless already changed)
            if ($action->getLocation() === $this) {
                $action->setLocation(null);
            }
        }

        return $this;
    }

    public function getTown(): ?Town
    {
        return $this->town;
    }

    public function setTown(Town $town): self
    {
        // set the owning side of the relation if necessary
        if ($town->getLocation() !== $this) {
            $town->setLocation($this);
        }

        $this->town = $town;

        return $this;
    }

    public function __toString(): string
    {
     $x = $this->getLocationx();
     $y = $this->getLocationy();
     return '('.$x.','.$y.')';
    }
}
