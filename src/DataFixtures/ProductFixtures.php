<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker\Factory;

class ProductFixtures extends Fixture
{


    public function __construct()
    {

    }

    public function load(ObjectManager $manager)
    {


        //create first dummy article

        $product = new Product();
        $product->setName('Units');
        $product->setPhoto("/images/unit.png");
        $product->setPrice('100');

        $manager->persist($product);

        //flush to database
        $manager->flush();



    }
}
