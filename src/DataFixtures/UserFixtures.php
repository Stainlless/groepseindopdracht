<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker\Factory;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $user = new User();
        $user->setEmail('barb@barb.com');
        $user->setUsername('Barbaar');
        $hashedPassword = $this->passwordEncoder->encodePassword($user, 'Adminpassword');
        $user->setPassword($hashedPassword);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword($hashedPassword);
        $user->setLogindate(new \DateTime(date("Y-m-d")));
        $user->setIsblocked(boolval(false));
        $user->setCoins(0);
        $user->setActionCount(0);

        $manager->persist(($user));

        $manager->flush();

        //create 10 fake users
        for ($i = 0; $i <= 10; $i++) {
        $user = new User();
        $user->setEmail($faker->email);
        $user->setUsername($faker->userName);
        $hashedPassword = $this->passwordEncoder->encodePassword($user, 'password');
        $user->setPassword($hashedPassword);
        $user->setRoles( ['ROLE_USER']);
        $user->setPassword($hashedPassword);
        $user->setLogindate(new \DateTime(date("Y-m-d")));
        $user->setIsblocked(boolval(false));
        $user->setCoins(rand(0,1000));
        $user->setActionCount(0);

        $manager->persist($user);
        }

        //flush to database
        $manager->flush();


        //create admin

        $user = new User();
        $user->setEmail('admin@admin.com');
        $user->setUsername('Admin');
        $hashedPassword = $this->passwordEncoder->encodePassword($user, 'Adminpassword');
        $user->setPassword($hashedPassword);
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword($hashedPassword);
        $user->setLogindate(new \DateTime(date("Y-m-d")));
        $user->setIsblocked(boolval(false));
        $user->setCoins(10000);
        $user->setActionCount(0);

        $manager->persist($user);

        //flush to database
        $manager->flush();
    }
}
