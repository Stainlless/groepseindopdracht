<?php

namespace App\DataFixtures;

use App\Entity\Action;
use App\Entity\Location;
use App\Entity\Product;
use App\Entity\Town;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Persistence\ObjectManager;

class DemoFixtures extends Fixture implements FixtureGroupInterface
{
    private $passwordEncoder;

    public static function getGroups(): array
    {
        return ['Demofixtures'];
    }

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        //Barbaar
        $user1 = new User();
        $user1->setEmail('barb@barb.com');
        $user1->setUsername('Barbaar');
        $hashedPassword = $this->passwordEncoder->encodePassword($user1, 'Adminpassword');
        $user1->setPassword($hashedPassword);
        $user1->setRoles(['ROLE_ADMIN']);
        $user1->setPassword($hashedPassword);
        $user1->setLogindate(new \DateTime(date("Y-m-d")));
        $user1->setIsblocked(boolval(false));
        $user1->setCoins(0);
        $user1->setActionCount(0);

        $manager->persist(($user1));

        //Owen
        $user2 = new User();
        $user2->setEmail('owie@owie.com');
        $user2->setUsername('darkheerser');
        $hashedPassword = $this->passwordEncoder->encodePassword($user2, 'TheBest');
        $user2->setPassword($hashedPassword);
        $user2->setRoles(['ROLE_ADMIN']);
        $user2->setPassword($hashedPassword);
        $user2->setLogindate(new \DateTime(date("Y-m-d")));
        $user2->setIsblocked(boolval(false));
        $user2->setCoins(100);
        $user2->setActionCount(10);

        $manager->persist(($user2));

        //Robbert-Jan
        $user3 = new User();
        $user3->setEmail('RJ@RJ.com');
        $user3->setUsername('Stainlless');
        $hashedPassword = $this->passwordEncoder->encodePassword($user3, 'fragomatic');
        $user3->setPassword($hashedPassword);
        $user3->setRoles(['ROLE_USER']);
        $user3->setPassword($hashedPassword);
        $user3->setLogindate(new \DateTime(date("Y-m-d")));
        $user3->setIsblocked(boolval(false));
        $user3->setCoins(290);
        $user3->setActionCount(18);

        $manager->persist(($user3));

        //Olivier
        $user4 = new User();
        $user4->setEmail('ollie@ollie.com');
        $user4->setUsername('Kuifje');
        $hashedPassword = $this->passwordEncoder->encodePassword($user4, 'Bobbie');
        $user4->setPassword($hashedPassword);
        $user4->setRoles(['ROLE_USER']);
        $user4->setPassword($hashedPassword);
        $user4->setLogindate(new \DateTime(date("Y-m-d")));
        $user4->setIsblocked(boolval(false));
        $user4->setCoins(570);
        $user4->setActionCount(990);

        $manager->persist(($user4));

        //flush to database
        $manager->flush();

        //locations
        $k = 1;
        for ($i = 0; $i < 20; $i++) {
            for ($j = 0; $j < 20; $j++) {
                $location[$k] = new Location();
                $location[$k]->setLocationx($i);
                $location[$k]->setLocationy($j);
                $manager->persist($location[$k]);
                $manager->flush();
                $k++;
            }
        }

        //towns met users gekoppeld en actie koppelen
        $town1 = new Town();
        $action1 = new Action();
        $town1->setLocation($location[202]);
        $town1->setTownquantity(90);
        $town1->setTownstatus('First town created');
        $town1->setUser($user3);
        $action1->setUser($user3);
        $action1->setLocation($location[202]);
        $action1->setActiondescription('First town was build');
        $action1->setTown($town1);
        $action1->setActionquantity(20);
        $manager->persist($town1);
        $manager->persist($action1);

        $town2 = new Town();
        $action2 = new Action();
        $town2->setLocation($location[204]);
        $town2->setTownquantity(90);
        $town2->setTownstatus('Town Build');
        $town2->setUser($user3);
        $action2->setUser($user3);
        $action2->setLocation($location[204]);
        $action2->setActiondescription('Build');
        $action2->setTown($town2);
        $action2->setActionquantity(rand(0, 100));
        $manager->persist($town2);
        $manager->persist($action2);

        $town3 = new Town();
        $action3 = new Action();
        $town3->setLocation($location[242]);
        $town3->setTownquantity(20);
        $town3->setTownstatus('Town Build');
        $town3->setUser($user3);
        $action3->setUser($user3);
        $action3->setLocation($location[242]);
        $action3->setActiondescription('Build');
        $action3->setTown($town3);
        $action3->setActionquantity(rand(0, 100));
        $manager->persist($town3);
        $manager->persist($action3);

        $town4 = new Town();
        $action4 = new Action();
        $town4->setLocation($location[79]);
        $town4->setTownquantity(50);
        $town4->setTownstatus('First town created');
        $town4->setUser($user4);
        $action4->setUser($user4);
        $action4->setLocation($location[79]);
        $action4->setActiondescription('First town build');
        $action4->setTown($town4);
        $action4->setActionquantity(20);
        $manager->persist($town4);
        $manager->persist($action4);

        $town5 = new Town();
        $action5 = new Action();
        $town5->setLocation($location[32]);
        $town5->setTownquantity(10);
        $town5->setTownstatus(' Town created');
        $town5->setUser($user4);
        $action5->setUser($user4);
        $action5->setLocation($location[32]);
        $action5->setActiondescription('Build');
        $action5->setTown($town5);
        $action5->setActionquantity(12);
        $manager->persist($town5);
        $manager->persist($action5);

        $town6 = new Town();
        $action6 = new Action();
        $town6->setLocation($location[200]);
        $town6->setTownquantity(100);
        $town6->setTownstatus('First town created');
        $town6->setUser($user2);
        $action6->setUser($user2);
        $action6->setLocation($location[200]);
        $action6->setActiondescription('First town build');
        $action6->setTown($town6);
        $action6->setActionquantity(100);
        $manager->persist($town6);
        $manager->persist($action6);

        $manager->flush();

        //product
        $product = new Product();
        $product->setName('Units');
        $product->setPhoto('/images/unit.png');
        $product->setPrice('100');

        $manager->persist($product);

        //flush to database
        $manager->flush();
    }
}
